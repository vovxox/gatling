package jira_performance

 

import scala.concurrent.duration._

 

import io.gatling.core.Predef._

import io.gatling.http.Predef._

import io.gatling.jdbc.Predef._

import scala.util.Random

 

 

 

class jira_performance extends Simulation {

 

  val httpProtocol = http

    .baseURL("http://localhost:8080")

    .inferHtmlResources()

    .acceptHeader("*/*")

    .acceptEncodingHeader("gzip, deflate, identity")

    .acceptLanguageHeader("de,en-US;q=0.7,en;q=0.3")

    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0")

    .doNotTrackHeader("1")

    .disableCaching

 

  val scenario1 = scenario("scenario 1 - normal").during(3 minutes) {

    exec(Jira.GoTo).exec(Jira.Login).exec(Jira.Create_Issue).exec(Jira.Create_Comment).exec(Jira.Logout)

  }

  val scenario2 = scenario("scenario 2 - peak").during(60 seconds) {

    exec(Jira.GoTo).exec(Jira.Login).exec(Jira.Search).exec(Jira.Logout)

  }

 

 

  object Jira {

 

    val GoTo = {

      group("index") {

        exec(http("JIRA")

          .get("/")

          .check(status.is(200)))

      }

      .pause(1)

    }

 

    val Login = {

      group("Login") {

        exec(http("Login")

          .post("/rest/gadget/1.0/login")

          .formParam("os_username", "admin")

          .formParam("os_password", "admin")

          .check(headerRegex("Set-Cookie", "atlassian.xsrf.token=(.*?);").find.saveAs("Token"))

          .check(status.is(200)))

      }

      .pause(1)

    }

 

    val Search = {

      group("Search_String") {

        exec(http("Suche Gatling")

            .post("/secure/QuickSearch.jspa")

            .formParam("searchString", "hooray")

            .check(status.is(200)))

          .pause(1)

 

          .exec(http("Search_filters")

            .get("/browse/TEST-1?filter=10200")

            .check(status.is(200)))

          .pause(1)

      }

    }

 

    val newTask = """ {

                  "fields": {

                    "project": {

                      "key": "TEST"

                    },

                    "summary": "This is summary",

                    "description": "This is description",

                    "issuetype": {

                      "name": "Task"

                    }

                  }

                }

                """

 

 

                val Create_Issue = {

                  group("CreateIssue") {

                    exec(http("CreateIssue")

                      .post("/rest/api/2/issue")

                      .header("Content-Type", "application/json")

                      .body(StringBody(newTask))

                      .asJSON

                      .check(jsonPath("$.id").saveAs("taskId"))

                      .check(status.is(201)))

                  }

                }

 

                val newComment = """ {

                               "body": "This is a test comment"

                             }

                             """

 

                             val Create_Comment = {

                               group("Commenting") {

                                 exec(http("Commenting")

                                   .post("/rest/api/2/issue/${taskId}/comment")

                                   .header("Content-Type", "application/json")

                                   .body(StringBody(newComment))

                                   .asJSON

                                   .check(status.is(201)))

                               }

                             }

 

                             val Logout = {

                               group("Logout") {

                                 exec(http("Logout")

                                   .get("/secure/Logout!default.jspa?atl_token=" + "${Token}"))

                               }

                             }

              }

 

              setUp(

                //Injects a given number of users with a linear ramp over a given duration.

                scenario1.inject(rampUsers(5) over(10 seconds))

                //Pause for a minute and throw a number of users at once

                scenario2.inject(nothingFor(60 seconds), atOnceUsers(25))

              ).protocols(httpProtocol)

}
