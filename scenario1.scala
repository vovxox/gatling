import scala.concurrent.duration._

 

import io.gatling.core.Predef._

import io.gatling.http.Predef._

import io.gatling.jdbc.Predef._

 

class Portfolio extends Simulation {

 

              val httpProtocol = http

                             .baseURL("http://jira")

                             .inferHtmlResources()

                            .acceptHeader("image/webp,image/apng,image/*,*/*;q=0.8")

                             .acceptEncodingHeader("gzip, deflate")

                             .acceptLanguageHeader("en-US,en;q=0.9")

                             .userAgentHeader("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36")

 

              val portfoliousers = scenario("Portfolio")

                             .exec(http("LoginPortfolio")

                                           .post("/rest/gadget/1.0/login")

                                           .formParam("os_username", "yippy")

                                           .formParam("os_password", "yippy")

                                           .formParam("os_cookie", "true")

                             )

                             .exec(http("Get Dashboard")

                                           .get("/secure/Dashboard.jspa")

                                           .check(status.is(200))

                             )

                             .exec(repeat(500, "n") {

                                           exec(http("Get DEA Portfolio Plan attempt ${n}")

                                                          .get("/secure/PortfolioPlanView.jspa?id=395"))

                                                          .pause(5)

                                           }

                             )

             

              val scrumboardusers = scenario("ScrumBoard")

                             .exec(http("LoginScrum")

                                           .post("/rest/gadget/1.0/login")

                                           .formParam("os_username", "yippykaye")

                                           .formParam("os_password", "yippykaye")

                                           .formParam("os_cookie", "true")

                             )

                             .exec(http("Get Dashboard")

                                           .get("/secure/Dashboard.jspa")

                                           .check(status.is(200))

                             )

                             .exec(repeat(1000, "i") {

                                           exec(http("Get Agile Board number ${i}")

                                                          .get("/secure/PortfolioPlanView.jspa?id=400" + "${i}"))

                                                          .pause(10)

                                           }

                             )

                            

              val adaptusers = scenario("Adaptavist")

                             .exec(http("LoginAdapt")

                                           .post("/rest/gadget/1.0/login")

                                           .formParam("os_username", "yippykaye2")

                                           .formParam("os_password", "yippykaye2")

                                           .formParam("os_cookie", "true")

                             )

                             .exec(http("Get Dashboard")

                                           .get("/secure/Dashboard.jspa")

                                           .check(status.is(200))

                             )

                             .exec(repeat(1000, "j") {

                                           exec(http("Get Adaptavist attempt ${j}")

                                                          .get("/secure/Tests.jspa#/design?projectId=17664"))

                                                          .pause(5)

                                           }

                             )

                            

             

              setUp(

                             portfoliousers.inject(rampUsers(15) over (120 seconds)),

                             scrumboardusers.inject(rampUsers(100) over (10 seconds)),

                            adaptusers.inject(rampUsers(30) over (10 seconds))

              ).protocols(httpProtocol)

}


